#include "bmp_funcs.h"

struct bmp_header create_header()
{
	return (struct bmp_header) {
		.bfType = 19778,
		.bfileSize = 0, 
		.bfReserved = 0, 
			.bOffBits = sizeof(struct bmp_header), 
			.biSize = 40, 
			.biWidth = 0, 
			.biHeight = 0, 
			.biPlanes = 1, 
			.biBitCount = 24, 
			.biCompression = 0, 
			.biSizeImage = 0, 
			.biXPelsPerMeter = 3780, 
			.biYPelsPerMeter = 3780, 
			.biClrUsed = 0, 
			.biClrImportant = 0 };
}

uint8_t get_padding(struct image const img)
{
	return (4 - (img.width * sizeof(struct pixel)) % 4) % 4;
}

enum read_status from_bmp(FILE* in, struct image* img)
{
	struct bmp_header head = create_header();
	
	if (fread(&head, sizeof(struct bmp_header), 1, in) != 1)
	{
		return READ_INVALID_HEADER;
	}

	*img = (struct image){ head.biWidth, head.biHeight, malloc(sizeof(struct pixel) * head.biWidth * head.biHeight) };

	uint8_t padding = get_padding(*img);
	for (uint32_t i = 0; i < img->height; i++)
	{
		if (fread(img->data + (i * img->width), sizeof(struct pixel), img->width, in) != img->width) { return READ_INVALID_BITS; };
		if (fseek(in, padding, SEEK_CUR)) { return READ_INVALID_SIGNATURE; }
	}
	return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const img)
{
	uint8_t padding = get_padding(img);
	uint32_t filesize = (img.width * sizeof(struct pixel) + padding) * img.height + sizeof(struct bmp_header);
	
	//struct bmp_header head = (struct bmp_header){ 19778, filesize, 0, sizeof(struct bmp_header), 40, img->width, img->height, 1, 24, 0, 0, 3780, 3780, 0, 0 };
	struct bmp_header head = create_header();
	head.bfileSize = filesize;
	head.biHeight = img.height;
	head.biWidth = img.width;
	head.bOffBits = sizeof(struct bmp_header);

	if (!fwrite(&head, sizeof(struct bmp_header), 1, out)) { return WRITE_ERROR; }
	
	for (uint32_t i = 0; i < img.height; i++)
	{
		if (fwrite(img.data + i * img.width, sizeof(struct pixel), img.width, out) != img.width) { return WRITE_ERROR; }
		if (fwrite(&padding, 1, padding, out) != padding) { return WRITE_ERROR; }
	}
	return WRITE_OK;
}