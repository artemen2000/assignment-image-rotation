#ifndef BMP_FUNCS
#define BMP_FUNCS
#include "bmp.h"
#include <malloc.h>
#include "image.h"



struct bmp_header create_header();

uint8_t get_padding(struct image const img);

enum read_status from_bmp(FILE* in, struct image* img);

enum write_status {
	WRITE_OK = 0,
	WRITE_ERROR
};

enum write_status to_bmp(FILE* out, struct image const img);

#endif