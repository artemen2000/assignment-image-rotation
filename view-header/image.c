#include "image.h"
#include "util.h"
#include "bmp_funcs.h"
#include <malloc.h>

struct image create_image()
{
	return (struct image) { .width = 0, .height = 0, .data = NULL };
}

enum read_status read_image(const char* filename, struct image* img) {
	FILE* f = fopen(filename, "rb");
	if (!f) return READ_INVALID_SIGNATURE;
	enum read_status stat = from_bmp(f, img);
	fclose(f);
	return stat;
}

bool write_image(const char* filename, struct image img) {
	FILE* f = fopen(filename, "wb");
	if (!f) return false;
	if (to_bmp(f, img) == WRITE_OK)
	{
		fclose(f);
		return true;
	}
	fclose(f);
	return false;
}

void free_image(struct image* img) {
	if (img->data != NULL) free(img->data);
}

uint32_t get_rotated_index(const uint32_t orig_index, const uint32_t orig_width, const uint32_t orig_height)
{
	uint32_t x = orig_index % orig_width;
	uint32_t y = orig_index / orig_width;
	uint32_t rot_x = y;
	uint32_t rot_y = orig_width - x - 1;
	return rot_y * orig_height + rot_x;
}

struct image rotate_image(struct image img) {
	struct image img_rot = create_image();
	img_rot.width = img.height;
	img_rot.height = img.width;
	img_rot.data = malloc((size_t)(sizeof(struct pixel) * img.width * img.height));

	for (uint32_t i = 0; i < img.width * img.height; i++)
	{
		img_rot.data[get_rotated_index(i, img.width, img.height)] = img.data[i];
	}
	return img_rot;
}