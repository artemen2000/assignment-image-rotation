#ifndef IMAGE_H
#define IMAGE_H
#include <inttypes.h>
#include <stdbool.h>
#include "bmp.h"

enum read_status {
	READ_OK = 0,
	READ_INVALID_SIGNATURE,
	READ_INVALID_BITS,
	READ_INVALID_HEADER
};

struct pixel { uint8_t b, g, r; };

struct image {
	uint32_t width;
	uint32_t height;
	struct pixel* data;
};

struct image create_image();
enum read_status read_image(const char* filename, struct image* img);
struct image rotate_image(struct image img);
bool write_image(const char* filename, struct image img);
void free_image(struct image* img);

#endif