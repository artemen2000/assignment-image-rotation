#include <stdbool.h>
#include <stdio.h>

#include "bmp.h"
#include "util.h"
#include "image.h"
#include "bmp_funcs.h"


void usage() {
    fprintf(stderr, "Usage: ./image_rotate BMP_FILE_NAME\n"); 
}

int main( int argc, char** argv ) {
    if (argc != 2) usage();
    if (argc < 2) err("Not enough arguments \n" );
    if (argc > 2) err("Too many arguments \n" );

    struct bmp_header h = { 0 };
    if (read_header_from_file( argv[1], &h )) {
        bmp_header_print( &h, stdout );
    }
    else {
        err( "Failed to open BMP file or reading header.\n" );
    }

    struct image img = create_image();

    if (read_image(argv[1], &img) != READ_OK) err("Failed to read image.");

    struct image rotated_image = rotate_image(img);
    
    if (!write_image("out.bmp", rotated_image)) err("Failed to write image.");
    free_image(&img);
    free_image(&rotated_image);
    getchar();
    return 0;
}
